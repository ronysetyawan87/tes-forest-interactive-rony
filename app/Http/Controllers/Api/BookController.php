<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class BookController extends Controller
{
    public function showBook(Request $request)
    {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        try {
            $body = '{ "bookId" : "'.$request->bookId.'" }';
            $getBook = $client->request('POST', 'http://103.197.59.202:3033/api/getBook', [
                'body' => $body
            ])->getBody()->getContents();

            $checkStock = $client->request('POST', 'http://103.197.59.202:3033/api/checkStock', [
                'body' => $body
            ])->getBody()->getContents();

            $qty = json_decode($checkStock)->quantity;
            if($qty > 0){
                $response = [
                    "bookId" => json_decode($getBook)->bookId,
                    "price" => json_decode($getBook)->price,
                    "displayPrice" => json_decode($getBook)->priceCurrency ." ".json_decode($getBook)->price,
                    "madeBy" => json_decode($getBook)->author,
                ];
            } else {
                $response = [
                    "status" => 222,
                    "description" => "Out of stock"
                ];
            }
        } catch (\Throwable $th) {
            $response = [
                "status" => 111,
                "description" => "Book ID not found"
            ];
        }

        // Kirim Hasil Ke
        // yoseph.wijaya@forest-interactive.com
        // bayu.waskita@wallet-codes.com

        return $response;
    }
}
